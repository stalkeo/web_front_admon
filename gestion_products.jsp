<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body>
		<label style = "font-size: 25px;">Gesti�n Productos</label>
		<div id = "contenedor_datos">
			<form method = "POST" action = "ControlServletAdmin">
				<div><input type="hidden" value="12" name="n_peticion" /></div> 
				<label>Busqueda Simple:</label>
				<div id='barra_busqueda'>
					<select name='category' style="height:30px;float: left;width: 20%;margin-right: 2%;" value ="Todo">
					  <option value='Todo'>Todo</option>
		              <option value='Tecnologia e Informatica'>Tecnolog�a e Inform�tica</option>
		              <option value='Coches y Motos'>Coches y Motos</option>
		              <option value='Deporte y Ocio'>Deporte y Ocio</option>
		              <option value='Muebles, Deco y Jardin'>Muebles, Deco y Jard�n</option>
		              <option value='Consolas y Videojuegos'>Consolas y Videojuegos</option>
		              <option value='Libros, Peliculas y Musica'>Libros, Pel�culas y M�sica</option>
		              <option value='Moda y Accesorios'>Moda y Accesorios</option>
		              <option value='Juguetes, Ni�os y Bebes'>Juguetes, Ni�os y Beb�s</option>
		              <option value='Inmobiliaria'>Inmobiliaria</option>
		              <option value='Electrodomesticos'>Electrodom�sticos</option>
		              <option value='Servicios'>Servicios</option>
		              <option value='Otros'>Otros</option>
					</select>
					<input type='text' name='search' style="height:19px;float: left;width: 60%;margin-right: 2%;" placeholder=''/>
					<input type ="submit" onclick="modificarLetra('Z')"value ="Busqueda" class = "button" type="button" >
				</div>
			</form>
			<form method = "POST" action = "ControlServletAdmin">
				<div><input type="hidden" value="13" name="n_peticion" /></div> 
				<label>Busqueda Avanzada:</label>
				<div id='barra_busqueda'>
					<center>
						<label>Categor�a:</label><select name='categoryAdv' style="height:30px;width: 20%;margin-right: 2%;margin-left: 1%;">
						  <option value='Todo'>Todo</option>
			              <option value='Tecnologia e Informatica'>Tecnolog�a e Inform�tica</option>
			              <option value='Coches y Motos'>Coches y Motos</option>
			              <option value='Deporte y Ocio'>Deporte y Ocio</option>
			              <option value='Muebles, Deco y Jardin'>Muebles, Deco y Jard�n</option>
			              <option value='Consolas y Videojuegos'>Consolas y Videojuegos</option>
			              <option value='Libros, Peliculas y Musica'>Libros, Pel�culas y M�sica</option>
			              <option value='Moda y Accesorios'>Moda y Accesorios</option>
			              <option value='Juguetes, Ni�os y Bebes'>Juguetes, Ni�os y Beb�s</option>
			              <option value='Inmobiliaria'>Inmobiliaria</option>
			              <option value='Electrodomesticos'>Electrodom�sticos</option>
			              <option value='Servicios'>Servicios</option>
			              <option value='Otros'>Otros</option>
						</select>
						<label>Provincia:</label><select name= "provincia" style="height:30px;width: 20%;margin-right: 2%;margin-left: 1%;">
							 <option value='' selected>Ciudad</option>
						 	 <option value='Alava'>Alava</option>
							 <option value='Albacete'>Albacete</option>
							 <option value='Alicante'>Alicante</option>
							 <option value='Almeria'>Almeria</option>
							 <option value='Asturias'>Asturias</option>
							 <option value='Avila'>Avila</option>
							 <option value='Badajoz'>Badajoz</option>
							 <option value='Barcelona'>Barcelona</option>
							 <option value='Burgos'>Burgos</option>
							 <option value='Caceres'>Caceres</option>
							 <option value='Cadiz'>Cadiz</option>
							 <option value='Cantabria'>Cantabria</option>
							 <option value='Castellonn'>Castellon</option>
							 <option value='Ceuta'>Ceuta</option>
							 <option value='Ciudad Real'>Ciudad Real</option>
							 <option value='Cordoba'>Cordoba</option>
							 <option value='Cuenca'>Cuenca</option>
							 <option value='Girona'>Girona</option>
							 <option value='Las Palmas'>Las Palmas</option>
							 <option value='Granada'>Granada</option>
							 <option value='Guadalajara'>Guadalajara</option>
							 <option value='Guipuzcoa'>Guipuzcoa</option>
							 <option value='Huelva'>Huelva</option>
							 <option value='Huesca'>Huesca</option>
							 <option value='Islas Baleares'>Islas Baleares</option>
							 <option value='Jaen'>Jaen</option>
							 <option value='A Coruna'>A Coruna</option>
							 <option value='La Rioja'>La Rioja</option>
							 <option value='Leon'>Leon</option>
							 <option value='Lleida'>Lleida</option>
							 <option value='Lugo'>Lugo</option>
							 <option value='Madrid'>Madrid</option>
							 <option value='Malaga'>Malaga</option>
							 <option value='Melilla'>Melilla</option>
							 <option value='Murcia'>Murcia</option>
							 <option value='Navarra'>Navarra</option>
							 <option value='Ourense'>Ourense</option>
							 <option value='Palencia'>Palencia</option>
							 <option value='Pontevedra'>Pontevedra</option>
							 <option value='Salamanca'>Salamanca</option>
							 <option value='Segovia'>Segovia</option>
							 <option value='Sevilla'>Sevilla</option>
							 <option value='Soria'>Soria</option>
							 <option value='Tarragona'>Tarragona</option>
							 <option value='Santa Cruz Tenerife'>Santa Cruz de Tenerife</option>
							 <option value='Teruel'>Teruel</option>
							 <option value='Toledo'>Toledo</option>
							 <option value='Valencia'>Valencia</option>
							 <option value='Valladolid'>Valladolid</option>
							 <option value='Vizcaya'>Vizcaya</option>
							 <option value='Zamora'>Zamora</option>
							 <option value='Zaragoza'>Zaragoza</option>
						</select>
					</center>
				</div>
				<div id='barra_busqueda'>
					<center>
						<label>Vendedor:</label><input type='text' name='vendedor' placeholder='Vendedor' style="width: 20%;margin-right: 1%;margin-left: 1%;">
						<label>T&#237;tulo:</label><input type='text' name='title' placeholder='T&#237;tulo' style="width: 20%;margin-right: 1%;margin-left: 1%;">
					</center>
				</div>
				<div id='barra_busqueda' style="height:60px;">
					<center>
						<label style='align-self: flex-start; padding-top: 5px;'>Descripci&#243;n:</label><textarea rows='2' cols='28' name='descrip' placeholder='Descripci&#243;n'></textarea>
					</center>
				</div>
				<div id='barra_busqueda'>
					<center>
						<input type ="submit" onclick="modificarLetra('Z')"value ="Busqueda avanzada" class = "button" type="button" style ="width: 30%">
					</center>
				</div>
			</form>
		</div>
		
		<c:set var="con_numrutas" value= "${0}"/>
		<c:set var="con_rutas" value= "${0}"/>
		
		<c:if test="${requestScope.allProducts != null}">
		    <c:forEach items="${requestScope.allProducts}" var="item">
			   <div id= "contenedor_datos_producto" >
					<div class= "producto" >
						<div class= "campo" >Titulo </div><div class="valor">${item.getTitulo()}</div>
						<div class= "campo" >Propietario: </div><div class="valor">${item.getUsuario().getEMail()}</div>
						<div class= "campo" >ID: </div><div class="valor">${item.getId()}</div>
						<div class= "campo" >Categor�a: </div><div class="valor">${item.getCategoria()}</div>
						<div class= "campo" >Estado: </div><div class="valor">${item.getEstado()}</div>
						<div class= "campo" >Precio: </div><div class="valor">${item.getPrecio()} </div>
						<div class= "campo" >Descripci�n: </div><div class="valor" style="">${item.getDescripcion()}</div>
						
						<input type= "button" onclick = "modificarBajaProduct('${item.getId()}','${item.getTitulo()}','${item.getUsuario().getEMail()}');mostrar(2)" value = "Dar de baja" class = "button" type="button" style = "width: 100px; height:30px; margin-top: 0; margin-left: 5%; margin-right: 3%;font-size: 12px; padding: 0; background-color: #e95f5a;text-align:center;float:right">
						<input type= "button" onclick = "modificarModProduct('${item.getId()}','${item.getUsuario().getEMail()}','${item.getCategoria()}','${item.getEstado()}','${item.getTitulo()}','${item.getDescripcion()}','${item.getPrecio()}');mostrar(1)" value = "Modificar" class = "button" type="button" style = "width: 100px; height:30px; margin-top: 0; margin-left: 0%; font-size: 12px; padding: 0; background-color: #80c2e4;text-align:center;float:right" >
						<c:if test="${requestScope.numrutas.get(con_numrutas) != null}">
		
								<c:if test="${requestScope.numrutas.get(con_numrutas) == 1}">
									<div id="imagenes">
										<a href="${requestScope.rutas.get(con_rutas)}" target="_blank"><img src="${requestScope.rutas.get(con_rutas)}" style="max-width: 22%;max-height: 400px;margin-bottom: 2%;"></a>
										<c:set var="con_rutas" value= "${con_rutas + 1}"/>
									</div>
								</c:if>
								<c:if test="${requestScope.numrutas.get(con_numrutas) == 2}">
									<div id="imagenes">
										<a href="${requestScope.rutas.get(con_rutas)}" target="_blank"><img src="${requestScope.rutas.get(con_rutas )}" style="max-width: 22%;max-height: 400px;margin-bottom: 2%;"></a>
										<a href="${requestScope.rutas.get(con_rutas + 1)}" target="_blank"><img src="${requestScope.rutas.get(con_rutas + 1)}" style="max-width: 22%;max-height: 400px;margin-bottom: 2%;"></a>
										<c:set var="con_rutas" value= "${con_rutas + 2}"/>
									</div>
								</c:if>
								<c:if test="${requestScope.numrutas.get(con_numrutas) == 3}">
									<div id="imagenes">
										<a href="${requestScope.rutas.get(con_rutas)}" target="_blank"><img src="${requestScope.rutas.get(con_rutas)}" style="max-width: 22%;max-height: 400px;margin-bottom: 2%;"></a>
										<a href="${requestScope.rutas.get(con_rutas + 1)}" target="_blank"><img src="${requestScope.rutas.get(con_rutas + 1)}" style="max-width: 22%;max-height: 400px;margin-bottom: 2%;"></a>
										<a href="${requestScope.rutas.get(con_rutas + 2)}" target="_blank"><img src="${requestScope.rutas.get(con_rutas + 2)}" style="max-width: 22%;max-height: 400px;margin-bottom: 2%;"></a>
										<c:set var="con_rutas" value= "${con_rutas + 3}"/>
									</div>
								</c:if>
								<c:if test="${requestScope.numrutas.get(con_numrutas) == 4}">
									<div id="imagenes">
										<a href="${requestScope.rutas.get(con_rutas)}" target="_blank"><img src="${requestScope.rutas.get(con_rutas)}" style="max-width: 22%;max-height: 400px;margin-bottom: 2%;"></a>
										<a href="${requestScope.rutas.get(con_rutas + 1)}" target="_blank"><img src="${requestScope.rutas.get(con_rutas + 1)}" style="max-width: 22%;max-height: 400px;margin-bottom: 2%;"></a>
										<a href="${requestScope.rutas.get(con_rutas + 2)}" target="_blank"><img src="${requestScope.rutas.get(con_rutas + 2)}" style="max-width: 22%;max-height: 400px;margin-bottom: 2%;"></a>
										<a href="${requestScope.rutas.get(con_rutas + 3)}" target="_blank"><img src="${requestScope.rutas.get(con_rutas + 3)}" style="max-width: 22%;max-height: 400px;margin-bottom: 2%;"></a>
										<c:set var="con_rutas" value= "${con_rutas + 4}"/>
									</div>
								</c:if>
								
								<c:set var="con_numrutas" value= "${con_numrutas + 1}"/>
	    				</c:if>
					</div>
				</div>
			</c:forEach>
	   	 </c:if>

		<div class = "fondo_modal" onclick = "Ocultar(1)">
		   <div class = "formulario" onclick = "cancelar()">
		     <form enctype="multipart/form-data" method = "POST" action = "ControlServletAdmin" class='contacto' style="height: 850px;">
		       <div id = "Titulo_logeo">Modificar producto</div>
		       <div><input type="hidden" value="14" name="n_peticion" /></div>
		       <div><input type="hidden" value="" name="e_mail_user" /></div>
		       <div id= "hidden_mod"><!-- ID de producto--></div>
		       <div><label>ID:</label><div id= "id_mod"></div></div>
		       <div><label>Propietario:</label><div id= "prop_mod"></div></div>
		       <div><label>Nombre del producto:</label><input type='text' name = "nombreP" value ='' id = "nombrePID" value = '' style="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0;" required></div>
			   <div><label style = "float: left;">Im&#225;gene principal del producto:</label><input type = 'file' accept = "image/*" name ="imagenP1" id = "imagenPID1" required style ="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0; float: left;"></div>
			   <div><label style = "float: left;">Im&#225;genes del producto:</label><input type = 'file' accept = "image/*" name ="imagenP2" id = "imagenPID2" style ="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0; float: left;"></div>
			   <div><label style = "float: left;">Im&#225;genes del producto:</label><input type = 'file' accept = "image/*" name ="imagenP3" id = "imagenPID3" style ="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0; float: left;"></div>
			   <div><label style = "float: left;">Im&#225;genes del producto:</label><input type = 'file' accept = "image/*" name ="imagenP4" id = "imagenPID4" style ="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0; float: left;"></div>
			   <div style = "float:left; font-weight: bold; margin-top: 25px;">Categor&#237;a</div>
			   <div style = "float: right; margin-top: 20px;">
					<select name='category_p' id='category_pid'style="height:25px; width: 150px;width: 70%;" value =''>
		              <option value='Tecnologia e Informatica'>Tecnolog�a e Inform�tica</option>
		              <option value='Coches y Motos'>Coches y Motos</option>
		              <option value='Deporte y Ocio'>Deporte y Ocio</option>
		              <option value='Muebles, Deco y Jardin'>Muebles, Deco y Jard�n</option>
		              <option value='Consolas y Videojuegos'>Consolas y Videojuegos</option>
		              <option value='Libros, Peliculas y Musica'>Libros, Pel�culas y M�sica</option>
		              <option value='Moda y Accesorios'>Moda y Accesorios</option>
		              <option value='Juguetes, Ni�os y Bebes'>Juguetes, Ni�os y Beb�s</option>
		              <option value='Inmobiliaria'>Inmobiliaria</option>
		              <option value='Electrodomesticos'>Electrodom�sticos</option>
		              <option value='Servicios'>Servicios</option>
		              <option value='Otros'>Otros</option>
					</select>
				</div>
			   <div style = "float:left; font-weight: bold; margin-top: 25px;">Estado</div>
			   <div style = "float: right; margin-top: 20px;">
					<select name='estado' id='estado_id' style="height:25px; width: 150px;" value =''>
						<option value='0'>Disponible</option>
			            <option value='1'>Reservado</option>
			            <option value='2'>Vendido</option>
					</select>
				</div>
				<div><label style = "float: left; margin-top: 15px;">Descripci&#243;n del producto</label><textarea name="Comentarios" cols="30" rows="3" maxlength="500" id = "comentario1" value ='' required ></textarea></div>
				<div style = "float:left; font-weight: bold; margin-top: 8px;">Precio (&#8364;)</div>
				<div style = "float: right;"><input type = 'number' name = "precioP" id = "precioPID" required value = '' style = "height:25px; width: 150px;" ></div>
				<div style = "clear: left;"><input type = 'submit' value = 'Dar de alta' style = "margin-top: 5px; margin-bottom: 20px; font-family: sans-serif; font-size: 12px; color: #798e94; float: left;"></div>
		     </form>
		   </div> 
		 </div>	
		 
		 <div class = "fondo_modal" onclick = "Ocultar(2)">
		   <div class = "formulario" onclick = "cancelar()">
		     <form method = "POST" action = "ControlServletAdmin" class='contacto'">
		       <div id = "Titulo_logeo"  style="font-size: 19px">Confirmaci�n baja producto</div>
		       <div><input type="hidden" value="15" name="n_peticion" /></div>
	 		   <div id= "hidden_baja"><!-- ID de producto--></div>
		       <div><label style="text-align:center;font-size:18px;margin-bottom: 1%">ID:</label><div id= "id_baja" style="text-align: center;font-size: 18px;font-weight: initial;"></div></div>
		       <div><label style="text-align:center;font-size:18px;margin-bottom: 1%">Titulo:</label><div id= "titulo_baja" style="text-align: center;font-size: 18px;font-weight: initial;"></div></div>
		       <div><label style="text-align:center;font-size:18px;margin-bottom: 1%">Propietario:</label><div id= "email_baja" style="text-align: center;font-size: 18px;font-weight: initial;"></div></div>
		       <div><center><input type = 'submit' value = 'Confirmar' style = "font-family: sans-serif; font-size: 12px; color: #798e94;"></center></div>
		     </form>
		   </div>
		</div>	
		 
	</body>
</html>