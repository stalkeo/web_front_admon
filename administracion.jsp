<%@page contentType="text/html" pageEncoding="UTF-8" language="java" autoFlush="true" buffer="12kb" session="true"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
        <title>El badulaque (Administración)</title>

        <!-- CSS files -->
        <link href='style/style-administracion.css' rel='stylesheet'>
        
        <!-- Favicon -->
        <link rel='icon' href='img/Wallapop.ico'>
        		
    </head>
    <body>
    
		<!-- CUERPO -->
		<div id = "medium">
		
			<div id = "head">
				<div id = "imagen_perfil">
					<span id = "texto_imagen_perfil">Bienvenido a la administración, <%=session.getAttribute("e_mail")%> </span>
				</div>		
			</div>
			
			<div id = "body_impostor">
			
				<div id = "opciones">			
					<div id = "menu_opciones">
						<form method = "POST" action = "ControlServletAdmin">
							<div><input type="hidden" value="2" name="n_peticion" /></div> 
							<input type ="submit" value ="Mis datos" class = "button" type="button">
						</form>
						<form method = "POST" action = "ControlServletAdmin">
							<div><input type="hidden" value="3" name="n_peticion" /></div>
							<div><input type="hidden" value="0" name="init_user" /></div>   
							<input type ="submit" value ="Gestión usuarios" class = "button" type="button">
						</form>
						<form method = "POST" action = "ControlServletAdmin">
							<div><input type="hidden" value="4" name="n_peticion" /></div> 
							<input type ="submit" value ="Gestión productos" class = "button" type="button">
						</form>
						<form method = "POST" action = "ControlServletAdmin">
							<div><input type="hidden" value="5" name="n_peticion" /></div> 
							<input type ="submit" value ="Chats" class = "button" type="button">
						</form>
						<form method = "POST" action = "ControlServletAdmin">
							<div><input type="hidden" value="6" name="n_peticion" /></div> 
							<input type ="submit" value ="Cerrar sesión" class = "button" type="button">
						</form>
					</div>
				</div>		
				
				<div id = "datos">
					<c:set var="include0" value="0"/>
					<c:if test="${sessionScope.Ventana == include0}">
			    		<%@ include file= "datos_admin.jsp" %>
					</c:if>			
					
					<c:set var="include1" value="1"/>
					<c:if test="${sessionScope.Ventana == include1}">
			    		<%@ include file= "gestion_users.jsp" %>
					</c:if>
					
					<c:set var="include2" value="2"/>
					<c:if test="${sessionScope.Ventana == include2}">
						<%@ include file= "gestion_products.jsp" %>
					</c:if>
					
					<c:set var="include3" value="3"/>
					<c:if test="${sessionScope.Ventana == include3}">
						<%@ include file= "chats.jsp" %>
					</c:if>
						
				</div>
				
				
			</div>					

		</div>
		<!-- SCRIPT -->
		<script type="text/javascript" src="script/administracion.js"></script>
		
		<c:if test="${requestScope.error == 2}">
			<script type="text/javascript">
	   			alert("La modificación no se ha podido realizar: Contraseña incorrecta");
	   		</script>
		</c:if>
		
		<c:if test="${requestScope.error == 3}">
			<script type="text/javascript">
	   			alert("Error en la modificación de los datos del administrador");
	   		</script>
		</c:if>
		
		<c:if test="${requestScope.error == 4}">
			<script type="text/javascript">
	   			alert("La baja no se ha podido realizar: Contraseña incorrecta");
	   		</script>
		</c:if>	
		
		<c:if test="${requestScope.error == 5}">
			<script type="text/javascript">
	   			alert("Error en la baja de la cuenta del administrador");
	   		</script>
		</c:if>		
		
		<c:if test="${requestScope.error == 6}">
			<script type="text/javascript">
	   			alert("La modificación no se ha podido realizar: Usuario existente");
	   		</script>
		</c:if>	
			
		<c:if test="${requestScope.error == 7}">
			<script type="text/javascript">
	   			alert("Error en la insercion de la nueva cuenta de usuario");
	   		</script>
		</c:if>
		<c:if test="${requestScope.error == 8}">
			<script type="text/javascript">
	   			alert("Error en la modificación del usuario seleccionado");
	   		</script>
		</c:if>		
		<c:if test="${requestScope.error == 9}">
			<script type="text/javascript">
	   			alert("Error en la eliminacion del usuario seleccionado");
	   		</script>
		</c:if>
		<c:if test="${requestScope.error == 10}">
			<script type="text/javascript">
	   			alert("No se han encontrado productos en la busqueda");
	   		</script>
		</c:if>		
		<c:if test="${requestScope.error == 11}">
			<script type="text/javascript">
	   			alert("Error en al dar de baja el producto selecionado");
	   		</script>
		</c:if>
		<c:if test="${requestScope.error == 11}">
			<script type="text/javascript">
	   			alert("No se ha podido enviar el mensaje, el usuario seleccionado no existe");
	   		</script>
		</c:if>			
		<c:if test="${requestScope.error == 12}">
			<script type="text/javascript">
	   			alert("No se ha podido enviar el mensaje, el usuario seleccionado no existe");
	   		</script>
		</c:if>
		<c:if test="${requestScope.error == 13}">
			<script type="text/javascript">
	   			alert("No tienes nuevos mensajes");
	   		</script>
		</c:if>							
    </body>
</html>