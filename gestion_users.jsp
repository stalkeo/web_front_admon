<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body>
		<label style = "font-size: 25px;">Gesti�n Usuarios<c:if test="${requestScope.letra_busqueda_A != null}"> (${requestScope.letra_busqueda_A})</c:if></label> 
		<button class = "button" type="button" onclick = "mostrar(1)" style = "width: 150px; height:30px; margin-top: 5px; margin-right: 0%; font-size: 13px; padding: 0; background-color: #80c2e4;float:right" >Registrar Usuario</button> 
		<div id= "busqueda_alfabetica">
			<form method = "POST" action = "ControlServletAdmin">
				<div><input type="hidden" value="3" name="n_peticion" /></div>
				<div><input type="hidden" value="0" name="init_user" /></div>  
				<div><input type="hidden" value="letra" id="letra_busqueda" name="letra_busqueda" /></div> 
				<input type ="submit" onclick="modificarLetra('A')"value ="a" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('B')"value ="b" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('C')"value ="c" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('D')"value ="d" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('E')"value ="e" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('F')"value ="f" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('G')"value ="g" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('H')"value ="h" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('I')"value ="i" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('J')"value ="j" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('K')"value ="k" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('M')"value ="m" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('N')"value ="n" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('L')"value ="l" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('O')"value ="o" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('P')"value ="p" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('Q')"value ="q" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('R')"value ="r" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('S')"value ="s" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('T')"value ="t" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('U')"value ="u" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('V')"value ="v" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('W')"value ="w" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('X')"value ="x" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('Y')"value ="y" class = "button" type="button" >
				<input type ="submit" onclick="modificarLetra('Z')"value ="z" class = "button" type="button" >
			</form>
		</div>
		<c:if test="${requestScope.allUser != null}">
		    <c:forEach items="${requestScope.allUser}" var="item">
			   <div id= "contenedor_datos" >
					<div class= "usuario" >
						<div class= "campo" >Email: </div><div class="valor">${item.getEMail()}</div>
						<div class= "campo" >Password: </div><div class="valor">${item.getPassword()}</div>
						<div class= "campo" >Nombre: </div><div class="valor">${item.getNombre()}</div>
						<div class= "campo" >1� Apellido: </div><div class="valor">${item.getApellido1()}</div>
						<div class= "campo" >2� Apellido: </div><div class="valor">${item.getApellido2()}</div>
						<div class= "campo" >Provincia: </div><div class="valor">${item.getCiudad()}</div>
						<div class= "campo" >C�digo Postal: </div><div class="valor">${item.getCodPostal()}</div>
						<div class= "campo" >Administrador: </div><div class="valor">${item.getAdministrador()}</div>
						
						<input type= "button" onclick = "modificarMod('${item.getEMail()}','${item.getPassword()}','${item.getNombre()}','${item.getApellido1()}','${item.getApellido2()}',${item.getCodPostal()},'${item.getAdministrador()}','${item.getCiudad()}');mostrar(2)" value = "Modificar" class = "button" type="button" style = "width: 100px; height:30px; margin-top: 0; margin-left: 0%; font-size: 12px; padding: 0; background-color: #80c2e4;text-align:center;float:left" >
						<input type= "button" onclick = "modificarBaja('${item.getEMail()}');mostrar(3)" value = "Dar de baja" class = "button" type="button" style = "width: 100px; height:30px; margin-top: 0; margin-left: 5%; font-size: 12px; padding: 0; background-color: #e95f5a;text-align:center;float:left">
					
					</div>
				</div>
			</c:forEach>
	    </c:if>
	    
	    <c:if test="${requestScope.init_user_A != null}">
		    <form method = "POST" action = "ControlServletAdmin">
				<div><input type="hidden" value="3" name="n_peticion" /></div>
				<div><input type="hidden" value=${requestScope.init_user_A} name="init_user" /></div>
				 <c:if test="${requestScope.letra_busqueda_A != null}">
				 	<div><input type="hidden" value=${requestScope.letra_busqueda_A} name="letra_busqueda" /></div>
				 </c:if>
				<input type ="submit" value ="Obtener m�s usuarios" class = "button" type="button" style="margin-top: 1%;margin-bottom: 1%;">
			</form>
		</c:if>
		
	    <div class = "fondo_modal" onclick = "Ocultar(1)">
	      <div class = "formulario" onclick = "cancelar()">
	        <form method = "POST" action = "ControlServletAdmin" class='contacto' onsubmit="return comprobarPassword()">
	          <div id = "Titulo_logeo">Registro</div>
	          <div><input type="hidden" value="9" name="n_peticion" /></div>
	          <c:if test="${requestScope.init_user_A != null}"><div><input type="hidden" value=${requestScope.init_user_A} name="init_user" /></div></c:if>
	          <c:if test="${requestScope.letra_busqueda_A != null}"><div><input type="hidden" value=${requestScope.letra_busqueda_A} name="letra_busqueda" /></div></c:if>
	          <div><label>E-mail:</label><input type='email' name = "e_mailR" id = "e_mail" required value = '' style="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0;"></div>
	          <div><label>Contrase&#241a:</label><input type='password' name = "contrasenaR" id = "pass1" value = '' required style="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0;"></div>
	          <div><label>Repita la contrase&#241a:</label><input type='password' name = "contrasenaR" id = "pass2" value = '' required style="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0;"></div>
	          <div><label>Nombre:</label><input type='text' name = "nombre" id = "pass" value = '' required></div>
	          <div><label>Primer apellido</label><input type='text' name = "apellido1" id = "apellido1" value = '' required></div>
	          <div><label>Segundo apellido</label><input type='text' name = "apellido2" id = "apellido2" value = ''></div>
	          <div><label>C&#243digo postal</label><input type='number' name = "cod_postal" id = "cod_postal" value = '' style="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0;"></div>
	          <div><div style = "float:left; font-weight: bold; margin-left: 3px;padding-top:3px">Permisos administraci�n </div><input type="checkbox" name="admin" value="true" style="margin-left:35%"></div>
	          <div style = "float:left; font-weight: bold; margin-left: 3px;">Provincia </div>
	          <div style = "float: right;">
	          <select name= "provincia" value="Madrid" style = "padding-left: 15px;">
				 	 <option value='�lava'>&#193lava</option>
					 <option value='Albacete'>Albacete</option>
					 <option value='Alicante'>Alicante</option>
					 <option value='Almer�a'>Almer&#237a</option>
					 <option value='Asturias'>Asturias</option>
					 <option value='�vila'>&#193vila</option>
					 <option value='Badajoz'>Badajoz</option>
					 <option value='Barcelona'>Barcelona</option>
					 <option value='Burgos'>Burgos</option>
					 <option value='C�ceres'>C&#225ceres</option>
					 <option value='C�diz'>C&#225diz</option>
					 <option value='Cantabria'>Cantabria</option>
					 <option value='Castell�n'>Castell&#243n</option>
					 <option value='Ceuta'>Ceuta</option>
					 <option value='Ciudad Real'>Ciudad Real</option>
					 <option value='C�rdoba'>C&#243rdoba</option>
					 <option value='Cuenca'>Cuenca</option>
					 <option value='Girona'>Girona</option>
					 <option value='Las Palmas'>Las Palmas</option>
					 <option value='Granada'>Granada</option>
					 <option value='Guadalajara'>Guadalajara</option>
					 <option value='Guip�zcoa'>Guip&#250zcoa</option>
					 <option value='Huelva'>Huelva</option>
					 <option value='Huesca'>Huesca</option>
					 <option value='Illes Balears'>Islas Baleares/Illes Balears</option>
					 <option value='Ja�n'>Ja&#233n</option>
					 <option value='A Coru�a'>A Coru&#241a</option>
					 <option value='La Rioja'>La Rioja</option>
					 <option value='Le�n'>Le&#243n</option>
					 <option value='Lleida'>Lleida</option>
					 <option value='Lugo'>Lugo</option>
					 <option value='Madrid'>Madrid</option>
					 <option value='M�laga'>M&#225laga</option>
					 <option value='Melilla'>Melilla</option>
					 <option value='Murcia'>Murcia</option>
					 <option value='Navarra'>Navarra</option>
					 <option value='Ourense'>Ourense</option>
					 <option value='Palencia'>Palencia</option>
					 <option value='Pontevedra'>Pontevedra</option>
					 <option value='Salamanca'>Salamanca</option>
					 <option value='Segovia'>Segovia</option>
					 <option value='Sevilla'>Sevilla</option>
					 <option value='soria'>Soria</option>
					 <option value='Tarragona'>Tarragona</option>
					 <option value='Santa Cruz Tenerife'>Santa Cruz de Tenerife</option>
					 <option value='Teruel'>Teruel</option>
					 <option value='Toledo'>Toledo</option>
					 <option value='Valencia'>Valencia/Val&#233ncia</option>
					 <option value='Valladolid'>Valladolid</option>
					 <option value='Vizcaya'>Vizcaya</option>
					 <option value='Zamora'>Zamora</option>
					 <option value='Zaragoza'>Zaragoza</option>
			</select></div>
	          <div><input type = 'submit' value = 'Acceder' style = "margin-bottom: 20px; font-family: sans-serif; font-size: 12px; color: #798e94; float: left;"></div>
	        </form>
	      </div> 
	    </div>
	    
	    <div class = "fondo_modal" onclick = "Ocultar(2)">
	      <div class = "formulario" onclick = "cancelar()">
	        <form method = "POST" action = "ControlServletAdmin" class='contacto' onsubmit="return comprobarPassword()">
	          <div id = "Titulo_logeo">Modificar usuario</div>
	          <div><input type="hidden" value="10" name="n_peticion" /></div>
	          <c:if test="${requestScope.init_user_A != null}"><div><input type="hidden" value=${requestScope.init_user_A} name="init_user" /></div></c:if>
	          <c:if test="${requestScope.letra_busqueda_A != null}"><div><input type="hidden" value=${requestScope.letra_busqueda_A} name="letra_busqueda" /></div></c:if>
	          <div><label>E-mail:</label><div id= "email_mod"></div></div> 
	          <div><div id= "hidden_mod"><!-- e_mailM --></div></div>   
	          <div><label>Contrase&#241a:</label><input type='text' name = "contrasenaM" id = "pass1M" value = '' required style="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0;"></div>
	          <div><label>Nombre:</label><input type='text' name = "nombreM" id = "nombreM" value = '' required></div>
	          <div><label>Primer apellido</label><input type='text' name = "apellido1M" id = "apellido1M" value = '' required></div>
	          <div><label>Segundo apellido</label><input type='text' name = "apellido2M" id = "apellido2M" value = ''></div>
	          <div><label>C&#243digo postal</label><input type='number' name = "cod_postalM" id = "cod_postalM" value = '' style="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0;"></div>
	          <div><div style = "float:left; font-weight: bold; margin-left: 3px;padding-top:3px">Permisos administraci�n </div><div id ="check_box_M"><!-- AdminM --></div></div>
	          <div style = "float:left; font-weight: bold; margin-left: 3px;">Provincia </div>
	          <div style = "float: right;">
	          <select name= "provinciaM" id="provinciaM" value="Madrid" style = "padding-left: 15px;">
				 	 <option value='�lava'>&#193lava</option>
					 <option value='Albacete'>Albacete</option>
					 <option value='Alicante'>Alicante</option>
					 <option value='Almer�a'>Almer&#237a</option>
					 <option value='Asturias'>Asturias</option>
					 <option value='�vila'>&#193vila</option>
					 <option value='Badajoz'>Badajoz</option>
					 <option value='Barcelona'>Barcelona</option>
					 <option value='Burgos'>Burgos</option>
					 <option value='C�ceres'>C&#225ceres</option>
					 <option value='C�diz'>C&#225diz</option>
					 <option value='Cantabria'>Cantabria</option>
					 <option value='Castell�n'>Castell&#243n</option>
					 <option value='Ceuta'>Ceuta</option>
					 <option value='Ciudad Real'>Ciudad Real</option>
					 <option value='C�rdoba'>C&#243rdoba</option>
					 <option value='Cuenca'>Cuenca</option>
					 <option value='Girona'>Girona</option>
					 <option value='Las Palmas'>Las Palmas</option>
					 <option value='Granada'>Granada</option>
					 <option value='Guadalajara'>Guadalajara</option>
					 <option value='Guip�zcoa'>Guip&#250zcoa</option>
					 <option value='Huelva'>Huelva</option>
					 <option value='Huesca'>Huesca</option>
					 <option value='Illes Balears'>Islas Baleares/Illes Balears</option>
					 <option value='Ja�n'>Ja&#233n</option>
					 <option value='A Coru�a'>A Coru&#241a</option>
					 <option value='La Rioja'>La Rioja</option>
					 <option value='Le�n'>Le&#243n</option>
					 <option value='Lleida'>Lleida</option>
					 <option value='Lugo'>Lugo</option>
					 <option value='Madrid'>Madrid</option>
					 <option value='M�laga'>M&#225laga</option>
					 <option value='Melilla'>Melilla</option>
					 <option value='Murcia'>Murcia</option>
					 <option value='Navarra'>Navarra</option>
					 <option value='Ourense'>Ourense</option>
					 <option value='Palencia'>Palencia</option>
					 <option value='Pontevedra'>Pontevedra</option>
					 <option value='Salamanca'>Salamanca</option>
					 <option value='Segovia'>Segovia</option>
					 <option value='Sevilla'>Sevilla</option>
					 <option value='soria'>Soria</option>
					 <option value='Tarragona'>Tarragona</option>
					 <option value='Santa Cruz Tenerife'>Santa Cruz de Tenerife</option>
					 <option value='Teruel'>Teruel</option>
					 <option value='Toledo'>Toledo</option>
					 <option value='Valencia'>Valencia/Val&#233ncia</option>
					 <option value='Valladolid'>Valladolid</option>
					 <option value='Vizcaya'>Vizcaya</option>
					 <option value='Zamora'>Zamora</option>
					 <option value='Zaragoza'>Zaragoza</option>
			</select></div>
	          <div><input type = 'submit' value = 'Acceder' style = "margin-bottom: 20px; font-family: sans-serif; font-size: 12px; color: #798e94; float: left;"></div>
	        </form>
	      </div> 
	    </div>
	    
		<div class = "fondo_modal" onclick = "Ocultar(3)">
		   <div class = "formulario" onclick = "cancelar()">
		     <form method = "POST" action = "ControlServletAdmin" class='contacto' onsubmit="return comprobarPassword()">
		       <div id = "Titulo_logeo"  style="font-size: 22px">Confirmaci�n de baja </div>
		       <div><input type="hidden" value="11" name="n_peticion" /></div>
		       <c:if test="${requestScope.init_user_A != null}"><div><input type="hidden" value=${requestScope.init_user_A} name="init_user" /></div></c:if>
	           <c:if test="${requestScope.letra_busqueda_A != null}"><div><input type="hidden" value=${requestScope.letra_busqueda_A} name="letra_busqueda" /></div></c:if>
		       <div id= "hidden_baja"><!-- e_mailB --></div>
		       <div><label style="text-align:center;font-size:18px;margin-bottom: 1%">E-mail:</label><div id= "email_baja" style="text-align: center;font-size: 18px;font-weight: initial;"></div></div>
		       <div><center><input type = 'submit' value = 'Confirmar' style = "font-family: sans-serif; font-size: 12px; color: #798e94;"></center></div>
		     </form>
		   </div>
		</div>	
	    
	</body>
</html>