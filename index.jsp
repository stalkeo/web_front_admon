<%@page contentType="text/html" pageEncoding="UTF-8" language="java" autoFlush="true" buffer="12kb" session="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
		<title>El badulaque (Administración)</title>
		
        <!-- CSS Index -->
        <link href="style/style-index.css" rel="stylesheet">
        
        <!-- Favicon -->
        <link rel="icon" href="img/Wallapop.ico">
	</head>
	
	<body>
		<div id =inicio_admin>
	   		<div class = "formulario">
			     <form method = "POST" action = "ControlServletAdmin" class='contacto' onsubmit="return comprobarPassword()">
			       <div id = "Titulo_logeo">Iniciar sesi&#243n administración</div>
			       <div><input type="hidden" value="1" name="n_peticion" /></div>
			       <div><label>E-mail:</label><input type='email' name = "e_mailL" id = "e_mail" required value = '' style="padding: 7px 6px;width: 100%;border: 1px solid #CED5D7;margin: 5px 0;"></div>
			       <div><label>Contrase&#241a:</label><input type='password' name = "contrasenaL" id = "pass" value = '' required style="padding: 7px 6px;width: 100%;border: 1px solid #CED5D7;margin: 5px 0;"></div>
			       <div><center><input type = 'submit' value = 'Acceder' style = "font-family: sans-serif; font-size: 18px; color: #798e94;"></center></div>
			     </form>
			</div>
		 </div>
		 
	 	<c:if test="${requestScope.error == 0}">
			<script type="text/javascript">
	   			alert("Error en el inicio de sesión: Email o contraseña invalida");
	   		</script>
		</c:if>
		<c:if test="${requestScope.error == 1}">
			<script type="text/javascript">
	   			alert("Su sesión ha caducado, por favor vuelva a logearse");
	   		</script>
		</c:if>		
	</body>
</html>