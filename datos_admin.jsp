
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body>
		<c:if test="${requestScope.user_data != null}">
			<label style = "font-size: 25px;">Mis datos</label>		
			<div id = "contenedor_datos">
				<form method = "POST" action = "ControlServletAdmin">
					<div><input type="hidden" value="7" name="n_peticion" /></div>
					<div class = "columna_datos">
						Nombre
						<input type="text" id="fname" name="nombre" value ="${user_data.getNombre()}" >
						Primer apellido
						<input type="text" id="fname" name="apellido1" value = "${user_data.getApellido1()}">
						Segundo apellido
						<input type="text" id="fname" name="apellido2" value = "${user_data.getApellido2()}">
						Codigo Postal
						<input type='number' name = "cod_postal" id = "cod_postal" value = "${user_data.getCodPostal()}">
					
					</div>
					
					<div class = "columna_datos">
						Contraseña Actual
						<input type="password" id="fname" name="pass_old"><br>
						Nueva contraseña
						<input type="password" id="fname" name="pass1"><br>
						Confirmar contraseña
						<input type="password" id="fname" name="pass2"><br>
						Ciudad de residencia
						<select name= "provincia" id = "provinciaID" value="${user_data.getCiudad()}">
							 <option value='${user_data.getCiudad()}'> ${user_data.getCiudad()}</option>
							 <option value='' selected>Ciudad</option>
						 	 <option value='Alava'>Alava</option>
							 <option value='Albacete'>Albacete</option>
							 <option value='Alicante'>Alicante</option>
							 <option value='Almeria'>Almeria</option>
							 <option value='Asturias'>Asturias</option>
							 <option value='Avila'>Avila</option>
							 <option value='Badajoz'>Badajoz</option>
							 <option value='Barcelona'>Barcelona</option>
							 <option value='Burgos'>Burgos</option>
							 <option value='Caceres'>Caceres</option>
							 <option value='Cadiz'>Cadiz</option>
							 <option value='Cantabria'>Cantabria</option>
							 <option value='Castellonn'>Castellon</option>
							 <option value='Ceuta'>Ceuta</option>
							 <option value='Ciudad Real'>Ciudad Real</option>
							 <option value='Cordoba'>Cordoba</option>
							 <option value='Cuenca'>Cuenca</option>
							 <option value='Girona'>Girona</option>
							 <option value='Las Palmas'>Las Palmas</option>
							 <option value='Granada'>Granada</option>
							 <option value='Guadalajara'>Guadalajara</option>
							 <option value='Guipuzcoa'>Guipuzcoa</option>
							 <option value='Huelva'>Huelva</option>
							 <option value='Huesca'>Huesca</option>
							 <option value='Islas Baleares'>Islas Baleares</option>
							 <option value='Jaen'>Jaen</option>
							 <option value='A Coruna'>A Coruna</option>
							 <option value='La Rioja'>La Rioja</option>
							 <option value='Leon'>Leon</option>
							 <option value='Lleida'>Lleida</option>
							 <option value='Lugo'>Lugo</option>
							 <option value='Madrid'>Madrid</option>
							 <option value='Malaga'>Malaga</option>
							 <option value='Melilla'>Melilla</option>
							 <option value='Murcia'>Murcia</option>
							 <option value='Navarra'>Navarra</option>
							 <option value='Ourense'>Ourense</option>
							 <option value='Palencia'>Palencia</option>
							 <option value='Pontevedra'>Pontevedra</option>
							 <option value='Salamanca'>Salamanca</option>
							 <option value='Segovia'>Segovia</option>
							 <option value='Sevilla'>Sevilla</option>
							 <option value='Soria'>Soria</option>
							 <option value='Tarragona'>Tarragona</option>
							 <option value='Santa Cruz Tenerife'>Santa Cruz de Tenerife</option>
							 <option value='Teruel'>Teruel</option>
							 <option value='Toledo'>Toledo</option>
							 <option value='Valencia'>Valencia</option>
							 <option value='Valladolid'>Valladolid</option>
							 <option value='Vizcaya'>Vizcaya</option>
							 <option value='Zamora'>Zamora</option>
							 <option value='Zaragoza'>Zaragoza</option>
						</select></div>
						
						<input type= "submit" value = "Actualizar" class = "button" type="button" style = "width: 100px; margin-top: 0; margin-left: 8%; font-size: 13px; padding: 0; background-color: #80c2e4;text-align:center" > 
						<button class = "button" type="button" onclick = "mostrar(1)" style = "width: 100px; margin-top: 0; margin-right: 8%; font-size: 13px; padding: 0; background-color: #e95f5a;float:right" >Dar de baja</button> 
					</form>
				</div>
				
			<div class = "fondo_modal" onclick = "Ocultar(1)">
			   <div class = "formulario" onclick = "cancelar()">
			     <form method = "POST" action = "ControlServletAdmin" class='contacto' onsubmit="return comprobarPassword()">
			       <div id = "Titulo_logeo">Confirmación de baja </div>
			       <div><input type="hidden" value="8" name="n_peticion" /></div>
			       <div><label>Contraseña:</label><input type='password' name = "contrasena" id = "pass" value = '' required style="padding: 7px 6px;width: 294px;border: 1px solid #CED5D7;margin: 5px 0;"></div>
			       <div style = "display:none"><label>Peticion</label> <input type = "hidden" name = "peticion" value = "Baja"></div>
			       <div><center><input type = 'submit' value = 'Confirmar' style = "font-family: sans-serif; font-size: 12px; color: #798e94;"></center></div>
			     </form>
			   </div>
			</div>
		</c:if>	 
	</body>
</html>